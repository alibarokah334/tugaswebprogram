import React, { Component } from 'react';
import { View, StyleSheet, Image, ScrollView } from 'react-native';

class FlexBox extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style = {styles.flexsatu}>
                    <Image source={require('/Users/User/terserahlagi/src/assets/atas.jpg')} style={styles.foto}/>
                </View>
                <View style = {styles.flexdua}>
                    <ScrollView> 
                    <Image source={require('/Users/User/terserahlagi/src/assets/story.jpg')} style={styles.cerita}/>
                    <Image source={require('/Users/User/terserahlagi/src/assets/gambar1.jpg')} style={styles.img}/>
                    <Image source={require('/Users/User/terserahlagi/src/assets/gambar2.jpg')} style={styles.img}/>
                    <Image source={require('/Users/User/terserahlagi/src/assets/gambar3.jpg')} style={styles.img}/>
                    <Image source={require('/Users/User/terserahlagi/src/assets/gambar4.jpg')} style={styles.img}/>
                    <Image source={require('/Users/User/terserahlagi/src/assets/gambar5.jpg')} style={styles.img}/>
                    </ScrollView>    
                </View>
                <View style = {styles.flextiga}>
                <Image source={require('/Users/User/terserahlagi/src/assets/bawah.jpg')} style={styles.ig}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: 'black'
    },
    flexsatu: {
        flex: 1, 
        backgroundColor: 'black'
    },
    flexdua: {
        flex: 9, 
        backgroundColor: 'black'
    },
    flextiga: {
        flex: 1, 
        backgroundColor: 'black'
    },
    foto: {
        height: 70, 
        width: 390
    },
    cerita: {
        height: 110,
        width: 410
    },
    img: {
        height: 670, 
        width: 425
    },
    ig: {
        height: 52, 
        width: 430
    },
});

export default FlexBox;